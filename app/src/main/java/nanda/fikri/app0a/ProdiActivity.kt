package nanda.fikri.app0a

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prodi.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class ProdiActivity : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsertP ->{
                queryInsertUpdateDelete("insert")
            }
            R.id.btnUpdateP ->{
                queryInsertUpdateDelete("update")
            }
            R.id.btnDeleteP ->{
                queryInsertUpdateDelete("delete")
            }
        }

    }

    lateinit var prodiAdapter : AdapterDataProdi
    var daftarProdi = mutableListOf<HashMap<String,String>>()
    val url = "http://192.168.43.157/kampus/get_nama_prodi.php"
    val url2 = "http://192.168.43.157/kampus/query_upd_del_ins_prodi.php"
    var id_prodi : String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prodi)
        prodiAdapter = AdapterDataProdi(daftarProdi, this)
        listProdi.layoutManager = LinearLayoutManager(this)
        listProdi.adapter = prodiAdapter

        btnInsertP.setOnClickListener(this)
        btnUpdateP.setOnClickListener(this)
        btnDeleteP.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        showDataProdi()
    }

    fun queryInsertUpdateDelete(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    edNamaProdi.setText("")
                    showDataProdi()
                }
                else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_prodi",id_prodi.toString())
                        hm.put("nama_prodi",edNamaProdi.text.toString())
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_prodi",id_prodi.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataProdi(){
        val request = StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarProdi.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("id_prodi",jsonObject.getString("id_prodi"))
                    prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
                    daftarProdi.add(prodi)
                }
                prodiAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}